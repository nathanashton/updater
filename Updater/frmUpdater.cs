﻿/*
 * Date: 11/02/2014
 * Time: 3:03 PM
 * 
 * Nathan Ashton
 */
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Xml.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.ComponentModel;
using System.Diagnostics;
using Ionic.Zip;


namespace Updater
{

    public partial class frmUpdater : Form
    {
       
        public enum quit
        {
            Normal,
            Fatal
        }
        
        Log log = new Log();
        UpdateDetails updateDetails  = new UpdateDetails();
        WebClient Client;
        
        public string arg_xmlLink { get; set; }
        public string arg_currentVersion { get; set; }
        public string arg_currentExecutablePath { get; set; }
        public string arg_currentExecutableEXE { get; set; }
        public bool arg_verbose { get; set; }

        string tempDirectory = Path.Combine(Path.GetTempPath(), "Updater_Temp-" + DateTime.Now.ToString("dd-MM-yyyy-HH-mm"));
        string backupDirectory = Path.Combine(Path.GetTempPath(), "Updater_Backup-" + DateTime.Now.ToString("dd-MM-yyyy-HH-mm"));
        string existingApplicationPath;
        
        
        List<string> existingApplicationFiles;
        List<string> existingApplicationRelativeFiles;
        List<string> existingFilesAfterBackup;
        List<string> UpdaterFilesList = new List<string>();
        
        
        #region Constructor
        public frmUpdater()
        {
            InitializeComponent();
            log.Info("********  Updater started  ********");
            
            string [] args = Environment.GetCommandLineArgs();
            
            try
            {
                arg_xmlLink = args[1];
                arg_currentVersion = args[2];
                arg_currentExecutablePath = args[3];
                arg_currentExecutableEXE = args[4];
                if (args.Length == 6)
                {
                    if (args[5] == "v" || args[5] == "V")
                    {
                        arg_verbose = true;
                    } else
                    {
                        arg_verbose = false;
                    }
                } else
                {
                    arg_verbose = false;
                }
                log.Info(String.Format("Received arguments arg_xmlLink: {0} arg_currentVersion: {1} arg_currentExecutablePath: {2} arg_currentExecutableEXE: {3} arg_verbose: {4}", arg_xmlLink, arg_currentVersion, arg_currentExecutablePath, arg_currentExecutableEXE, arg_verbose));
                if (arg_verbose)
                {
                    log.Info("VERBOSE Logging enabled");
                }
            } catch (Exception ex)
            {
                log.Error("Required arguments are missing or invalid!", ex); 
                MessageBox.Show("Required arguments are missing or invalid!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Quit(quit.Fatal);
            }
            
           
            // MANUAL OVERRIDES for testing purposes
            //arg_xmlLink = @"http://nathanashton.synology.me:8000/update.xml";
            //arg_currentVersion = "1.4.0.0";
            //arg_currentExecutablePath = @"e:\git\Updater\Updater\bin\Debug\Updater.exe";
            //arg_currentExecutableEXE = "Updater.exe";
            //arg_verbose = true;               
            
            
            #region Paths
            // Existing application base directory.
            existingApplicationPath = Path.GetDirectoryName(arg_currentExecutablePath);
            log.Info(String.Format("Existing Application Path: " + existingApplicationPath));

            // Full path of existing application files.
            existingApplicationFiles = this.GetApplicationFiles(existingApplicationPath); 
            
            // Relative path of existing application files.
            existingApplicationRelativeFiles = new List<string>();
            foreach (string existingFile in existingApplicationFiles)
            {
                string relative = existingFile.Remove(existingFile.IndexOf(existingApplicationPath), existingApplicationPath.Length);
                existingApplicationRelativeFiles.Add(relative);
            }
            
            existingFilesAfterBackup = new List<string>();
            foreach ( string s in existingApplicationRelativeFiles)
            {
                string n = backupDirectory + s;
                existingFilesAfterBackup.Add(n);
            }
            
            
            if (arg_verbose)
            {
                foreach (string s in existingApplicationFiles)
                {
                    log.Info(String.Format("Existing Application File: " + s));
                }
                
                foreach (string s in existingApplicationRelativeFiles)
                {
                   log.Info(String.Format("Relative path to Existing Files: " + s));
                }
                            
                foreach ( string s in existingFilesAfterBackup)
                {
                   log.Info(String.Format("Location after backup: " + s));
                }
            }
            
            
            UpdaterFilesList.Add(existingApplicationPath + "\\Updater.exe");
            UpdaterFilesList.Add(existingApplicationPath + "\\Ionic.Zip.dll");
            UpdaterFilesList.Add(existingApplicationPath + "\\updater.log");
            #endregion
            
            this.Begin();
        }
        #endregion
         
        
        #region Methods
        void Begin()
        {
            try
            {
                this.ReadXMLFile();
            } catch (Exception ex)
            {
                MessageBox.Show("Cannot connect to the update service.\n\n" + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Quit(quit.Fatal);
            }
            
                if (this.IsUpdateNewer())
                {
                    frmConfirmUpdate confirm = new frmConfirmUpdate();
                    confirm.in_details = updateDetails;
                    DialogResult result = confirm.ShowDialog();
                        
                    switch (result)
                    {
                        case DialogResult.OK:
                            try
                            {
                                this.BackupExistingFiles(existingApplicationFiles);  
                            } catch (Exception)
                            {
                                MessageBox.Show("There was an error backing up existing files", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                this.Quit(quit.Fatal);
                            }
                            try
                            {
                                this.Download();
                            } catch (Exception)
                            {
                                MessageBox.Show("There was an error downloading the update", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                this.Quit(quit.Fatal);
                            }
                            break;
                        
                        case DialogResult.Cancel:
                            this.Quit(quit.Normal);
                            break;
                    }
                } else
                {
                    MessageBox.Show("No updates are available", "Update", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    log.Info("No udpates are available");
                    this.Quit(quit.Normal);
                }
        }
        
        
        bool IsUpdateNewer()
        {
            var currentVersion = new Version(arg_currentVersion);
            var newVersion = new Version(updateDetails.version);
            log.Info(String.Format("Current version {0} - Update version {1}", currentVersion.ToString(), newVersion.ToString()));
            var result = currentVersion.CompareTo(newVersion);
            if (result < 0)
            {
                return true;
            } else
            {
                return false;
            }
        }
        
        
        void ReadXMLFile()
        {
            XmlDocument doc = new XmlDocument();
            try
            {
                doc.Load(arg_xmlLink);
                XmlNodeList nodes = doc.DocumentElement.SelectNodes("/UpdateDetails");
                foreach (XmlNode node in nodes)
                {
                    updateDetails.name = node.SelectSingleNode("name").InnerText;
                    updateDetails.downloadurl = node.SelectSingleNode("downloadurl").InnerText;
                    updateDetails.version = node.SelectSingleNode("version").InnerText;
                    updateDetails.md5= node.SelectSingleNode("md5").InnerText;
                    updateDetails.filename = node.SelectSingleNode("filename").InnerText;
                    if (node.SelectSingleNode("readme").InnerText != null)
                    {
                        updateDetails.readme = node.SelectSingleNode("readme").InnerText;
                    }
                }
                log.Info(String.Format("XML contents: " + updateDetails.ToString()));
            } catch (Exception ex)
            {
                log.Error(String.Format("Cannot connect to the update service! at " + arg_xmlLink) , ex);
                throw;
            }
        }
        
        
        void Download()
        {
            this.Text = updateDetails.name;
            Directory.CreateDirectory(tempDirectory);
            log.Info(String.Format("Temp update directory created at " + tempDirectory));
            try
            {
                using (Client = new WebClient ())
                {
                    log.Info("Download started");
                    Client.DownloadFileCompleted += new AsyncCompletedEventHandler(DownloadCompleted);
                    
                    Client.DownloadProgressChanged += new DownloadProgressChangedEventHandler(ProgressChanged); 
                    Client.DownloadFileAsync(new Uri(updateDetails.downloadurl), tempDirectory + "//" + updateDetails.filename);
                }
            } catch (Exception ex)
            {
                log.Error("Error downloading update file", ex);
                throw;
            }

        }
        
        string MD5Hash(string fileName)
        {
            FileStream file = new FileStream(fileName, FileMode.Open);
            System.Security.Cryptography.MD5 md5 = new System.Security.Cryptography.MD5CryptoServiceProvider();
            byte[] retVal = md5.ComputeHash(file);
            file.Close();
         
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < retVal.Length; i++)
            {
                sb.Append(retVal[i].ToString("x2"));
            }
            return sb.ToString().ToUpper();
        }
        
        
        List<string> GetApplicationFiles(string in_path)
        {
            List<string> allfiles = new List<string>();
            foreach (string file in Directory.EnumerateFiles(in_path, "*.*", SearchOption.AllDirectories))
            {
                allfiles.Add(file);
            }
            return allfiles;
        }
        
        
        // TODO needs cleanup and refining
        void BackupExistingFiles(List<string> in_path)
        {
            try
            {

                for (int i = 0; i < in_path.Count; i++)
                {
                    Directory.CreateDirectory(Path.GetDirectoryName(existingFilesAfterBackup[i]));
                    File.Copy(in_path[i], existingFilesAfterBackup[i], true);
                    log.Info(String.Format("Backed up file: " + existingFilesAfterBackup[i]));
                }
            log.Info("Existing application files backed up successfully");    
            } catch (Exception ex)
            {
                log.Error("Error backup up existing files", ex);
                throw;
            }
        }
        
        
        void DeleteExistingFiles(List<string> in_files)
        {
            //TODO Doesnt delete folders. Need to delete folders as well
            try
            {
                foreach(string f in in_files)
                {
                     if (!UpdaterFilesList.Contains(f))
                     {
                         File.Delete(f);
                         log.Info(String.Format("Existing Application file deleted: " + f));
                     }
                } 
            log.Info("Existing application files deleted successfully");
            } catch (Exception ex)
            {
              log.Error("Error deleting existing application files", ex);
              throw;
            }
        }
        
        
        void ProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            progressBar1.Value = e.ProgressPercentage;
            label1.Text = "Downloading Version " + updateDetails.version + "......." + e.ProgressPercentage.ToString() + "%";
        }
        
        
        void DownloadCompleted(object sender, AsyncCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                log.Info("Download cancelled");
                MessageBox.Show("Download cancelled", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                try
                {
                    Directory.Delete(tempDirectory, true);
                    log.Info(String.Format(tempDirectory + " deleted"));
                    Directory.Delete(backupDirectory, true);                    
                    log.Info(String.Format(backupDirectory + " deleted"));
                    this.Quit(quit.Normal);
                } catch (Exception ex)
                {
                    MessageBox.Show("Error deleting the temporary directories.\n\n" + existingApplicationPath + "\\" + tempDirectory +"\n\n" + existingApplicationPath + "\\" + backupDirectory, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    log.Info(String.Format("Error deleting the temporary directories " + tempDirectory), ex);
                    this.Quit(quit.Fatal);
                } 
            } else
            {
            
                log.Info("Download complete");
                string MD5Hash = this.MD5Hash(Path.Combine(tempDirectory, updateDetails.filename));
    
                if (MD5Hash == updateDetails.md5)
                {
                    
                //label1.Text = "Updating " + updateDetails.name + " to Version: " + updateDetails.version;
                log.Info("MD5 hashes match");
                
                    try
                    {
                        this.UnzipUpdate();
                    } catch (Exception)
                    {
                        MessageBox.Show("There was an error extracting the update file!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        this.Quit(quit.Fatal);
                    }
                    
                    try
                    {
                        this.DeleteExistingFiles(existingApplicationFiles);
                        this.CopyFiles(tempDirectory, existingApplicationPath); 
                        
                    } catch (Exception)
                    {
                        MessageBox.Show("There was an error updating the application files", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        this.Quit(quit.Fatal);
                    }
                    
                    try
                    {
                       // Delete temporary Directories
                        (new DirectoryInfo(tempDirectory)).Delete(true);
                        log.Info(String.Format(tempDirectory + " deleted"));                    
                        (new DirectoryInfo(backupDirectory)).Delete(true);
                        log.Info(String.Format(backupDirectory + " deleted"));
                    }  catch (Exception ex)
                    {
                        log.Error("There was an error deleting the temporary directories", ex);
                    }
                    
                    // No exceptions found so update has been completed successfully.
                    MessageBox.Show("Application successfully updated", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    log.Info("Application successfully updated");
                    
                    try
                    {
                        Process p = new Process();
                        p.StartInfo.FileName = arg_currentExecutableEXE;
                        p.Start();
                        this.Quit(quit.Normal);
                    } catch (Exception ex)
                    {
                        MessageBox.Show("There was an error starting " + arg_currentExecutableEXE +"\n\nYou can start the program manually", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        log.Error(String.Format("There was an error restarting the calling application! - " + arg_currentExecutableEXE), ex);
                        this.Quit(quit.Normal);
                    } 
                
                } else
                {
                    MessageBox.Show("There was an error with the downloaded update file.\n\n Please try downloading it manually from " + updateDetails.downloadurl.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    log.Error("There was an error with the downloaded file, MD5 hashes don't match!");
                    this.Quit(quit.Fatal);
                }
            }
        }
        
        /*
        void KillApp(string in_exeName)
        {
            foreach (var process in Process.GetProcessesByName(in_exeName))
            {
                try
                {
                    process.Kill();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }*/
        
        
        void UnzipUpdate()
        {
            string zipToUnpack = Path.Combine(tempDirectory,updateDetails.filename);
            string unpackDirectory = tempDirectory;
            try
            {
                using (ZipFile zip1 = ZipFile.Read(zipToUnpack))
                {
                    foreach (ZipEntry e in zip1)
                    {
                        e.Extract(unpackDirectory, ExtractExistingFileAction.OverwriteSilently);
                        log.Info(String.Format("Extracted " + unpackDirectory + "\\" + e.FileName + " successfully"));
                    }
                }
                log.Info(String.Format("Update " + updateDetails.version + " extracted successfully"));
            } catch (Exception ex)
            {
                log.Error("Error extracting update file", ex);
                throw;
            }
        }
        
        
        void CopyFiles(string sourceFolder, string destFolder)
        {

           if (!Directory.Exists(destFolder))
           {
                Directory.CreateDirectory(destFolder);          
           }

            string[] files = Directory.GetFiles(sourceFolder);  
            try
            {
                foreach (string file in files)
                {  
                    string name = Path.GetFileName(file);  
                    string dest = Path.Combine(destFolder, name);  
                    File.Copy(file, dest);  
                }
            } catch (IOException ex)
            {
                log.Error(String.Format("Error copying files from " + sourceFolder + " to " + destFolder), ex);
            }
            
            string[] folders = Directory.GetDirectories(sourceFolder);  
            try
            {
            foreach (string folder in folders)
            {  
                string name = Path.GetFileName(folder);  
                string dest = Path.Combine(destFolder, name);  
                CopyFiles(folder, dest);  
            }  
            } catch (IOException ex)
            {
                log.Error(String.Format("Error copying directories from " + sourceFolder + " to " + destFolder), ex);
            }
        }
        
        
        void Quit(quit in_param)
        {
            switch (in_param)
            {
                case quit.Normal:
                    log.Info("Updater closed normally");
                    //TODO Should probalby check for update and backup directories and cleanup
                    Environment.Exit(0);
                    break;
                
                case quit.Fatal:
                    
                    try
                    {
                        Directory.Delete(tempDirectory, true);
                        log.Info("Temporary update files have been deleted");
                    } catch (Exception ex)
                    {
                        log.Info(String.Format("There was an error deleting temporary files: " + ex.ToString()));
                    }
                    
                    //TODO Restore from the backup folder and cleanup                     

                    MessageBox.Show("Updater exited with a fatal error. All files have been restored.\n\nPlease check updater.log for more details.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    log.Error("Updater closed with a fatal error");
                    Environment.Exit(0);     
                    break;
            }
        }
        
        
        void BtnCancelClick(object sender, EventArgs e)
        {
            Client.CancelAsync();
        }
                #endregion
    }
    
}

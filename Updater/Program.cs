﻿/*
 * Date: 11/02/2014
 * Time: 3:03 PM
 * 
 * Nathan Ashton
 */
using System;
using System.Windows.Forms;

namespace Updater
{
    /// <summary>
    /// Class with program entry point.
    /// </summary>
    internal sealed class Program
    {
        /// <summary>
        /// Program entry point.
        /// </summary>
        [STAThread]
        private static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new frmUpdater());
            
            
        }
        
    }
}

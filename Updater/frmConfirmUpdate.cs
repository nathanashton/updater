﻿/*
 * Date: 17/02/2014
 * Time: 8:23 AM
 * 
 * Nathan Ashton
 */
using System;
using System.Drawing;
using System.Windows.Forms;
using System.IO;
using System.Net;

namespace Updater
{
    /// <summary>
    /// Description of frmConfirmUpdate.
    /// </summary>
    public partial class frmConfirmUpdate : Form
    {
        

        public UpdateDetails in_details;
                    Log log = new Log();
        
        
        public frmConfirmUpdate()
        {
            InitializeComponent();

        }
        
        void BtnYesClick(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;            
        }
        
        void BtnNoClick(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;            
        }
        
        void FrmConfirmUpdateLoad(object sender, EventArgs e)
        {
            label2.Text = "Version " + in_details.version + " available";
            this.Text = in_details.name;
            try
            {
            using (var Client = new WebClient())
            {
                if (in_details.readme != null)
                {
                  textBox1.Text = Client.DownloadString(in_details.readme);  
                } else
                {
                    textBox1.Text = "No release notes available";
                }

            }
            } catch (Exception ex)
            {
                log.Error("Could not find readme file", ex);
                textBox1.Text = "Could not find specified readme file or no readme file exists";
            }
        }
    }
}

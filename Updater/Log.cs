﻿/*
 * Date: 13/02/2014
 * Time: 10:02 AM
 * 
 * Nathan Ashton
 */
using System;
using System.IO;

namespace Updater
{

    public class Log
    {
        
        public StreamWriter logFile;
        
        
        string Now()
        {
            return DateTime.Now.ToString();
        }
        
        
        void Open()
        {
            logFile = new StreamWriter("updater.log", true);
        }
        
        
        void Close()
        {
            if (logFile != null)
            {
                logFile.Close();
            }
        }
        
        
        public void Error(string in_message, Exception in_exception)
        {

            this.Open();
            logFile.WriteLine(Now() + ": ERROR: " + in_message + ": " + in_exception.ToString());
            this.Close();
        }
        
        
        public void Error(string in_message)
        {
            this.Open();
            logFile.WriteLine(Now() + ": ERROR: " + in_message);
            this.Close();
        }
        
        
        public void Info(string in_message)
        {
            this.Open();
            logFile.WriteLine(Now() + ": INFO: " + in_message);
            this.Close();
        }
        
        
        public void Info(string in_message, Exception in_exception)
        {
            logFile.WriteLine(Now() + ": INFO: " + in_message + ": " + in_exception.ToString());
        }
    }
}

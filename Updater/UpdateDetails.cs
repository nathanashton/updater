﻿using System;
using System.Xml.Serialization;
using System.IO;
using System.Net;
using System.Security.Cryptography;

namespace Updater
{

    public class UpdateDetails
    {
        
        public string name { get; set; }
        public string downloadurl { get; set; }
        public string version { get; set; }
        public string md5 { get; set; }
        public string filename { get; set; }
        public string readme { get; set; }
        
       
        

        
        
        public override string ToString()
        {
            return name + " " + downloadurl + " " + version + " " + md5;
        }
    }
}

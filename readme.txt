Updater is is called with 4 arguments.
1. Link to XML Update file
2. Current version of App
3. Current Application Path
4. Current Applicaiton EXE

There is an option 5th argument to enable more detailed logging
5. "V" or "v" to enable verbose logging


Process n = new Process();
n.StartInfo.FileName = "Updater.exe";
n.StartInfo.Arguments = xmllink + " " + currentversion + " " + applicationpath + " " + applicationexe;
n.Start();